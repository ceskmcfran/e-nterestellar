import React from "react";
import "./App.css";
import { BrowserRouter, Route } from "react-router-dom";
import Layout from "./Layout";
import Dashboard from "../pages/Dashboard";
import Email from "../pages/Email";
import Data from "../pages/Data";
import News from "../pages/News";
import Obituary from "../pages/Obituary";

export default function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Route exact path="/" component={Dashboard}></Route>
        <Route exact path="/email" component={Email}></Route>
        <Route exact path="/data" component={Data}></Route>
        <Route exact path="/news" component={News}></Route>
        <Route exact path="/obituary" component={Obituary}></Route>
      </Layout>
    </BrowserRouter>
  );
}
