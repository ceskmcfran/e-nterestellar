import React, { useState } from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  makeStyles,
} from "@material-ui/core";

const useStyles = makeStyles({
  root: {},
});

const Mailbox = ({ className, ...rest }) => {
  const classes = useStyles();
  const [values, setValues] = useState({
    to: "",
    confirm: "",
  });

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <form className={clsx(classes.root, className)} {...rest}>
      <Card>
        <CardHeader subheader="Send an email!" title="Mail" />
        <Divider />
        <CardContent>
          <TextField
            fullWidth
            label="To"
            margin="normal"
            name="To"
            onChange={handleChange}
            value={values.to}
            variant="outlined"
          />
          <TextField
            fullWidth
            label="Body"
            margin="normal"
            name="confirm"
            onChange={handleChange}
            multiline={true}
            value={values.body}
            variant="outlined"
          />
        </CardContent>
        <Divider />
        <Box display="flex" justifyContent="flex-end" p={2}>
          <Button color="primary" variant="contained">
            Send
          </Button>
        </Box>
      </Card>
    </form>
  );
};

Mailbox.propTypes = {
  className: PropTypes.string,
};

export default Mailbox;
