import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Chart from "../components/Chart";
import CardNews from "../components/CardNews";
import { Copyright } from "../components/Copyright";
import Traffic from "../components/Traffic";
import TasksProgress from "../components/TasksProgress";

function createData(time, amount) {
  return { time, amount };
}

const data = [
  createData("00:00", 0),
  createData("03:00", 300),
  createData("06:00", 600),
  createData("09:00", 800),
  createData("12:00", 1500),
  createData("15:00", 2000),
  createData("18:00", 2400),
  createData("21:00", 2400),
  createData("24:00", undefined),
];

const featuredPosts = [
  {
    title: "Earth is contaminated with a global disease",
    date: "Mar 21",
    description: "SARS-COV is the new desease in planet Earth...",
    image: "https://source.unsplash.com/random",
    imageText: "COVID",
  },
  {
    title: "US calls for a capital advance to companies",
    date: "Mar 19",
    description: "After predicting a global pandemic, United States asks...",
    image: "https://source.unsplash.com/random",
    imageText: "USA",
  },
];

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  news: {
    marginLeft: -4,
    marginRight: -4,
  },
}));

export default function Dashboard(props) {
  const classes = useStyles();

  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    <Container maxWidth="lg" className={classes.container}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={fixedHeightPaper}>
            <Chart
              title={"Resources Summary"}
              xAxis={"Iron (Units)"}
              data={data}
            />
          </Paper>
        </Grid>
        <Grid container spacing={4} className={classes.news}>
          {featuredPosts.map((post) => (
            <CardNews key={post.title} post={post} />
          ))}
        </Grid>
        <Grid item lg={12} sm={12} xl={12} xs={12}>
          <TasksProgress />
        </Grid>
        <Grid item lg={4} md={6} xl={3} xs={12}>
          <Traffic />
        </Grid>

        {/* 
        <Grid item xs={12} md={4} lg={3}>
          <Paper className={fixedHeightPaper}>
            <Deposits />
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Orders />
          </Paper>
        </Grid>*/}
      </Grid>
      <Box pt={4}>
        <Copyright />
      </Box>
    </Container>
  );
}
