import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import GitHubIcon from "@material-ui/icons/GitHub";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import Header from "../components/Header";
import MainCard from "../components/MainCard";
import CardNews from "../components/CardNews";
import Main from "../components/Main";
import Sidebar from "../components/Sidebar";
import post1 from "../posts/blog-post-1.md";
import post2 from "../posts/blog-post-2.md";
import post3 from "../posts/blog-post-3.md";
import { Copyright } from "../components/Copyright";
import Box from "@material-ui/core/Box";

const useStyles = makeStyles((theme) => ({
  mainGrid: {
    marginTop: theme.spacing(3),
  },
}));

const sections = [
  { title: "Technology", url: "#" },
  { title: "Design", url: "#" },
  { title: "Culture", url: "#" },
  { title: "Business", url: "#" },
  { title: "Politics", url: "#" },
  { title: "Opinion", url: "#" },
  { title: "Science", url: "#" },
  { title: "Health", url: "#" },
  { title: "Style", url: "#" },
  { title: "Travel", url: "#" },
];

const mainFeaturedPost = {
  title: "",
  description:
    "The new platform for communication between Earth and Mars is now active for all those citizens of both planets.",
  image: "https://source.unsplash.com/random",
  imgText: "main image description",
  linkText: "Continue reading…",
};

const featuredPosts = [
  {
    title: "Earth is contaminated with a global disease",
    date: "Mar 21",
    description: "SARS-COV is the new desease in planet Earth...",
    image: "https://source.unsplash.com/random",
    imageText: "COVID",
  },
  {
    title: "US calls for a capital advance to companies",
    date: "Mar 19",
    description: "After predicting a global pandemic, United States asks...",
    image: "https://source.unsplash.com/random",
    imageText: "USA",
  },
];

const posts = [post1, post2, post3];

const sidebar = {
  title: "About",
  description: "Keep up with all the news from Earth and Mars.",
  archives: [
    { title: "March 2020", url: "#" },
    { title: "February 2020", url: "#" },
    { title: "January 2020", url: "#" },
    { title: "November 1999", url: "#" },
    { title: "October 1999", url: "#" },
    { title: "September 1999", url: "#" },
    { title: "August 1999", url: "#" },
    { title: "July 1999", url: "#" },
    { title: "June 1999", url: "#" },
    { title: "May 1999", url: "#" },
    { title: "April 1999", url: "#" },
  ],
  social: [
    { name: "GitHub", icon: GitHubIcon },
    { name: "Twitter", icon: TwitterIcon },
    { name: "Facebook", icon: FacebookIcon },
  ],
};
export default function News() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg">
        <Header title="" sections={sections} />
        <main>
          <MainCard post={mainFeaturedPost} />
          <Grid container spacing={4}>
            {featuredPosts.map((post) => (
              <CardNews key={post.title} post={post} />
            ))}
          </Grid>
          <Grid container spacing={5} className={classes.mainGrid}>
            <Main title="From the firehose" posts={posts} />
            <Sidebar
              title={sidebar.title}
              description={sidebar.description}
              archives={sidebar.archives}
              social={sidebar.social}
            />
          </Grid>
        </main>
      </Container>
      <Box pt={4}>
        <Copyright />
      </Box>
    </React.Fragment>
  );
}
