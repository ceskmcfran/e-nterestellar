import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Profile from "../components/Profile";
import avatar6 from "../../assets/avatar_6.png";
import avatar1 from "../../assets/avatar_1.png";
import avatar2 from "../../assets/avatar_2.png";
import avatar3 from "../../assets/avatar_3.png";
import avatar10 from "../../assets/avatar_10.png";
import avatar11 from "../../assets/avatar_11.png";
import { Copyright } from "../components/Copyright";

const users = [
  {
    id: 1,
    avatar: avatar6,
    city: "Sactuary",
    jobTitle: "Mantainer",
    name: "Katarina Smith",
    date: "03/11/2019 11:47:14 PM",
  },
  {
    id: 2,
    avatar: avatar2,
    city: "Rigel",
    jobTitle: "Engeneer",
    name: "Jane Fort",
    date: "01/21/2019 03:03:22 PM",
  },
  {
    id: 3,
    avatar: avatar1,
    city: "Jeel",
    jobTitle: "Mantainer",
    name: "John Doe",
    date: "06/28/2019 04:39:25 AM",
  },
  {
    id: 4,
    avatar: avatar3,
    city: "Gran Marcian",
    jobTitle: "Teacher",
    name: "Michael Doe",
    date: "03/17/2019 01:08:01 AM",
  },
  {
    id: 5,
    avatar: avatar10,
    city: "Canyon Road",
    jobTitle: "Explorer",
    name: "Diane Max",
    date: "07/27/2020 12:40:34 PM",
  },
  {
    id: 6,
    avatar: avatar11,
    city: "Mars NY",
    jobTitle: "Engeneer",
    name: "Claire Herz",
    date: "01/05/2019 04:57:22 PM",
  },
];

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
}));

export default function Obituary(props) {
  const classes = useStyles();

  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    <Container maxWidth="lg" className={classes.container}>
      <Grid container spacing={3}>
        {users.map((user) => (
          <Grid item lg={4} md={6} xs={12}>
            <Profile user={user} />
          </Grid>
        ))}
      </Grid>
      <Box pt={4}>
        <Copyright />
      </Box>
    </Container>
  );
}
