const Path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");

module.exports = {
  entry: {
    app: Path.resolve(__dirname, "src/index.js"),
  },
  output: {
    path: Path.resolve(__dirname, "dist"),
    filename: "js/[name].js",
    publicPath: "http://localhost:9000/",
  },
  devServer: {
    port: 9000,
    hot: true,
    open: true,
    contentBase: Path.resolve(__dirname, "dist"),
    historyApiFallback: {
      disableDotRule: true,
    },
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: "babel-loader",
        exclude: "/node_modules/",
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.md$/,
        use: [
          {
            loader: "html-loader",
          },
          {
            loader: "webpack-md-loader",
            options: {
              html: true,
            },
          },
        ],
      },
      {
        test: /\.jpg|png|gif|woff|eot|ttf|svg|mp4|webm$/,
        exclude: "/node_modules/",
        use: {
          loader: "file-loader",
          options: { outputPath: "assets/" },
        },
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: Path.resolve(__dirname, "public/index.html"),
      favicon: "./public/favicon.ico",
    }),
  ],
};
